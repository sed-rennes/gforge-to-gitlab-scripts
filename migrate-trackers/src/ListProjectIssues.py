#!/usr/bin/env python3

import os
import sys
import getopt
import re
import requests
import urllib.parse
from lxml import etree
from io import StringIO,BytesIO
from datetime import datetime
import json
from gitlab_connection import gitlab_connection

# Everything here can be configured from the command line.

gitlabProjectName = 'some_group/some_project'
gitlab_api_url = 'https://gitlab.inria.fr/api/v4'
personalAccessToken = None

def print_help():
    help_text="""
Usage: ListProjectIssues.py [options]

Recognized options:
    --accesstoken=<gitlabPersonalAccessToken>
    --gitlabprojectname=<projectname or project id>
    --gitlabserver=<gitlab api url, e.g. https://gitlab.example.com/api/v4>

"""
    print(help_text)

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                "hdt:p:g:",
                [
                    "debug",
                    "accesstoken=",
                    "gitlabprojectname=",
                    "gitlabserver=",
                    ])
    except getopt.GetoptError as e:
        print(e)
        print_help()
        sys.exit(2)

    debug = False

    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-t", "--accesstoken"):
            personalAccessToken = arg
        elif opt in ("-p", "--gitlabprojectname"):
            gitlabProjectName = arg
        elif opt in ("--gitlabserver"):
            gitlab_api_url = arg
        elif opt in ("-d", "--debug"):
            debug = True

    gl = gitlab_connection(gitlab_api_url, personalAccessToken, debug_api_calls=debug)

    pr = gl.project(gitlabProjectName)

    for x in pr.get_paginated("/issues"):
        print(x['iid'])
