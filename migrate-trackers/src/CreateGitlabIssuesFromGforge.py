#!/usr/bin/env python3

import os
import sys
import getopt
import re
import requests
import urllib.parse
from lxml import etree
from io import StringIO,BytesIO
from datetime import datetime
import json

from gitlab_connection import gitlab_connection
from gitlab_project_connection import gitlab_project_connection
from user_translator import user_translator

# Everything here can be configured from the command line.

gforge_toplevel = 'https://gforge.inria.fr/'
gforge_group_id=999999

gitlabProjectName = 'some_group/some_project'
restrict_issues = None
dry_run = True
stealth = False
gitlab_api_url = 'https://gitlab.inria.fr/api/v4'
personalAccessToken = None



# encoding used in the gforge pages -- need it for csv parsing (which
# isn't terribly useful, to be honest, but we'll use it nevertheless. For
# one, it's a handy way to get all issues in one go and not navigate
# through many pages, which would induce complicated logic).  the
# encoding is set by the first html parse.
encoding=None


all_issue_iids=[]

tracker_to_label = {
        'Bugs': 'bug',
        'FeatureRequests': 'feature-request',
}

priorityToLabel = {
        '1': '',
        '2': 'low-priority',
        '3': '',    # default -- avoid clutter, do not have a label for that
        '4': 'high-priority',
        '5': 'top-priority',
}

labels_always=[ 'imported from gforge' ]

def report_error(text):
    return "\x1b[01;31m{}\x1b[00m".format(text)

def looks_good(text):
    return "\x1b[01;32m{}\x1b[00m".format(text)


def gforge_url(function=None, tracker_id=None, issue_id=None, **kwargs):
    global gforge_toplevel
    s = gforge_toplevel + "tracker/" + "?"
    ss = []
    global gforge_group_id
    ss.append("group_id=%d" % int(gforge_group_id))
    if function is not None:
        ss.append("func=%s" % function)
    if tracker_id is not None:
        ss.append("atid=%d" % int(tracker_id))
    if issue_id is not None:
        ss.append("aid=%d" % int(issue_id))
    for k,v in kwargs.items():
        ss.append("%s=%s" % (k,v))
    s = s + "&".join(ss)
    return s

def unquote(s):
    mm=re.match("\"(.*)\"", s, re.DOTALL)
    if mm:
        return mm.group(1)
    else:
        return s

def get_list_of_trackers():
    """
        reutrns an array [ tracker_id, tracker_name ] with all trackers
        found in the project
    """

    page = requests.get(gforge_url())

    if page.status_code != 200:
        raise RuntimeError("Cannot list trackers: %s" % page.reason)

    tree=etree.parse(BytesIO(page.content),etree.HTMLParser())

    global encoding
    encoding=tree.docinfo.encoding

    return [
            (int(re.search("atid=(\d+)",x.attrib['href']).group(1)),
             re.sub('\s','',"".join(list(x.itertext()))))
            for x in tree.findall(
                ".//table[@id=\"sortable_table_tracker\"]/tbody/tr/td/a")
            ]

def get_terse_descriptions_of_issues_for_tracker(tracker):
    """
        returns an array of issues, each given as a hash with all fields
        set as we read them from the csv
    """
    tracker_id,tracker_name=tracker
    url=gforge_url('downloadcsv',
            tracker_id,
            headers=1,
            sep="\x00",
            _status=100,
            _assigned_to=0,
            _sort_col='artifact_id',
            set='custom'
            )
    req=requests.get(url)
    if req.status_code != 200:
        raise RuntimeError("Cannot get csv for %s: %s" %
                (tracker_name, req.reason))

    data=req.iter_lines()
    # first lines contains all headers.
    header_line=next(data).decode(encoding)
    headers=header_line.split("\x00")
    # gforge mixes up \n as a line separator and \n inside the issue
    # text, so that we can only find the true line beginnings by regexps
    # such as the following.
    issues=[]
    current=[]
    for l in data:
        ls=l.decode(encoding)
        if re.match("^\d+\x00.*",ls):
            if current:
                issues.append(current)
            current=[]
        current.append(ls)
    if current:
        issues.append(current)
    issues=["\n".join(i).split("\x00") for i in issues]
    issues_hashes=[]
    for iss in issues:
        assert(len(headers)==len(iss))
        n=len(headers)
        ii={}
        for i in range(n):
            ii[headers[i]]=unquote(iss[i])
        issues_hashes.append(ii)
    return issues_hashes

def get_detail_page(issue_id):
    """
    This gets the "normal" gforge issue page, whose url is of the form:
    https://gforge.inria.fr/tracker?aid=123456
    (this odes get redirected to something different, but the format
    above is the minimal one).
    We return the page content as an xml tree
    """
    url=gforge_url('detail', issue_id=issue_id)
    req=requests.get(url)
    if req.status_code != 200:
        raise RuntimeError("Cannot get detail for issue %d: %s" %
                (issue_id, req.reason))

    tree=etree.parse(BytesIO(req.content),etree.HTMLParser())

    return tree

def get_all_messages_of_issue(issue_id, tree=None):
    if tree is None:
        tree = get_detail_page(issue_id)
    root=tree.getroot()
    messages=root.findall(".//table[@id=\"messages_list\"]/tbody/tr/td")
    print("Found %d messages in issue id %d" % (len(messages), issue_id))
    ret=[]
    for m in messages:
        pieces = m.findall("./")
        assert len(pieces) == 2
        assert pieces[0].tag == 'span'
        assert pieces[1].tag == 'p'
        date=re.sub("^Date: ","",pieces[0].text)
        su = pieces[0].find("./a")
        if su is not None:
            sender = su.text
        else:
            sender = "gforge_nobody"
        text="\n".join(pieces[1].itertext())
        mh={    'date':date,
                'sender':sender,
                'text':text,
                'issue_id':issue_id,
        }
        ret.append(mh)
    # this is unimportant if the user doing the operation has owner
    # access level and can change the dates. But if it is *not* the case,
    # we'd rather arrange so that the ordering looks right, and not in
    # the wrong order! (yes, it's possible to tweak the gforge request
    # page to that end as well)
    ret.reverse()
    return ret

def get_all_attachments_of_issue(issue_id, tree=None):
    if tree is None:
        tree = get_detail_page(issue_id)
    root=tree.getroot()
    attachments=root.findall(".//div[@id=\"tabber-attachments\"]//tbody/tr")
    print("Found %d attachments in issue id %d" % (len(attachments), issue_id))
    ret=[]
    for a in attachments:
        pieces=["\n".join(x.itertext()) for x in a.findall("./td")]
        (size,title,date,user,linkpath)=pieces
        link=gforge_toplevel + re.sub("^/","", a.find(".//a").attrib['href'])
        ret.append({
            'date': date,
            'sender': user,
            'title': title,
            'source': link,
            'basename': linkpath,
        })
    return ret

def extract_more_fields_from_detail_page(issue, tree):
    """
    This function fills a few fields that we know how to get _only_ from the
    gforge detail page, not from the csv.
    """
    root=tree.getroot()
    # third row in table looks like
    #  <td><strong>Submitted by:</strong><br />Joe Reporter(<samp><a href="/users/jrep/">jrep</a></samp>)</td><td><strong>Assigned to:</strong><br />Poor Assignee (passign) </td>
    row3=root.findall(".//form[@id=\"trackerdetailform\"]/table/tr[3]/td")
    if not row3:
        raise RuntimeError("Cannot get submitter for gforge issue id {}".format(issue['artifact_id']))
    su = row3[0].find("./samp/a")
    if su is not None:
        issue['submitter_unixname'] = su.text
    else:
        issue['submitter_unixname'] = "gforge_nobody"
    foo=re.search("\((\w+)\)",str(etree.tostring(row3[1])))
    if foo:
        issue['assigned_to_unixname'] = foo.group(1)

def print_list_of_trackers(trackers):
    print("Trackers:")
    for id,t in trackers:
        print("\t%s ; atid=%d" % (t, id))

def print_title_of_issues(issues, where):
    print("Found %d issues%s" % (len(issues), where))
    for issue in issues:
        issue_id=int(issue['artifact_id'])
        summary=issue['summary']
        print("\t%d: %s" % (issue_id, summary))

def convertDate(gforgeDate):
    # dateObject = datetime.strptime(gforgeDate,'%d/%m/%Y %H:%M')
    dateObject = datetime.strptime(gforgeDate,'%Y-%m-%d %H:%M')
    return dateObject.isoformat()

def transform_issue(tracker,issue,translator):
    tracker_id,tracker_name=tracker
    # print(issue)
    issue['iid']=int(issue['artifact_id'])
    issue['gforge_url']=gforge_url(issue_id=issue['iid'])
    author=issue['submitter_name']
    submitter_login=issue.get('submitter_unixname')

    ass=translator.translate('gitlab_uid',
            gforge_name=issue['assigned_to_name'],
            gforge_login=issue['assigned_to_unixname'],
            )

    if ass is not None:
        issue['assignee_ids'] = [ ass ]
    else:
        print(report_error("Issue is assigned to nobody, as {} is unknown".format(issue['assigned_to_name'])))

    who = translator.translate(
            gforge_name=author,
            gforge_login=submitter_login)
    if who is None or 'gitlab_login' not in who:
        who=author
    else:
        issue['gitlab_login']=who['gitlab_login']
        if 'gitlab_uid' in who:
            issue['gitlab_uid']=who['gitlab_uid']
        who="[{}](@{})".format(who['gforge_name'], who['gitlab_login'])

    issue['description']=\
        '__Imported issue:__ Initially reported by ' + who + ' in ' + \
        issue['gforge_url'] + "\n---\n" + issue['details']

    labels=[]
    for l in labels_always:
        labels.append(l)
    t=tracker_to_label.get(tracker_name)
    if t is not None:
        labels.append(t)
    l=priorityToLabel[issue['priority']]
    if l:
        labels.append(l)
    issue['labels']=labels
    issue['open_date']=convertDate(issue['open_date'])
    issue['last_modified_date']=convertDate(issue['last_modified_date'])

    # Look up the authors of messages and attachments **even in
    # dry-run mode**.

    issue['posts']=[]
    for m in issue['messages']:
        sender = m['sender']
        who=translator.translate(gforge_name=sender)
        if who is None or 'gitlab_login' not in who:
            who=sender
        else:
            m['gitlab_login']=who['gitlab_login']
            if 'gitlab_uid' in who:
                m['gitlab_uid']=who['gitlab_uid']
            who="[{}](@{})".format(who['gforge_name'], who['gitlab_login'])
        m['translated_sender']=who
        m['type']='message'
        m['date']=convertDate(m['date'])
        issue['posts'].append(m)

    for a in issue['attachments']:
        sender = a['sender']
        who=translator.translate(gforge_login=sender)
        if who is None or 'gitlab_login' not in who:
            who=sender
        else:
            a['gitlab_login']=who['gitlab_login']
            if 'gitlab_uid' in who:
                a['gitlab_uid']=who['gitlab_uid']
            who="[{}](@{})".format(who['gforge_name'], who['gitlab_login'])
        a['translated_sender']=who
        a['type']='attachment'
        a['date']=convertDate(a['date'])
        issue['posts'].append(a)

    del issue['messages']
    del issue['attachments']

    # alpha-sort is fine with iso8601 dates!
    issue['posts'].sort(key=lambda x:x['date'])


def create_issue(pr, issue):
    """
    This creates the issue on the gitlab project that is represented by
    the object pr
    """
    
    if not pr.has_owner_access():
        # prepend this, since otherwise it might be hard to recover the
        # original issue id.
        issue['summary'] = "(gforge {}) {}".format(issue['iid'], issue['summary'])

    api_keys=dict(
        title=issue['summary'],
        labels=",".join(issue['labels']),
        created_at=issue['open_date'],
        iid=issue['iid'],
        description=issue['description'].replace('\n','\n\n'))

    if 'assignee_ids' in issue:
        api_keys['assignee_ids'] = issue['assignee_ids']

    def can_impersonate(pr, x):
        """
        test whether we can run sudo to make comments appear as
        originating from the real poster. This is currently disabled. The
        reason is that while it's indeed possible to change the reporting
        user (if he/she is a member of the project), it becomes
        impossible to modify the dates of the comments (not supported by
        the gitlab notes api, as of gitlab 13.6).
        """
        return False
        if not pr.has_sudo_access():
            return False
        if 'gitlab_uid' not in x:
            return False
        t = pr.visibility == "public" or x['gitlab_uid'] in pr.members
        return t

    if can_impersonate(pr, issue):
        api_keys['sudo'] = issue['gitlab_login']

    issue_id=issue['iid']
    summary=issue['summary']
    print("\tcreating issue %d: %s" % (issue_id, summary))
    req = pr.post("/issues", expected_return=[201], **api_keys)

    # This id might differ from the ID we tried to set!
    server_id = req['iid']

    if server_id != issue_id:
        print("Note: gforge id {} is now gitlab id {}".format(issue_id, server_id))

    for m in issue['posts']:
        sender = m['sender']
        who = m['translated_sender']
        where = "on [gforge tracker]({})".format(issue['gforge_url'])

        kwargs=dict()
        if can_impersonate(pr, m):
            kwargs['sudo']=m['gitlab_login']

        if m['type'] == 'message':
            what = m['text']
            text =  '__Posted by {} {}__\n---\n{}'.format(who, where, what)

            print("\tadding message from %s to issue %d: %s" % (sender, server_id, summary))
        elif m['type'] == 'attachment':
            basename=m['basename']

            # a['source'] is the path to the file on gforge.
            req=requests.get(m['source'])
            if req.status_code != 200:
                raise RuntimeError("Cannot download {} from {} (status_code={}\n{})".format(basename, m['source'], req.status_code, req.content.decode("UTF-8")))
            r=BytesIO(req.content)

            # post this as an upload to the project
            jj=pr.post("/uploads", files={'file': (basename,r) }, **kwargs)
            # note that it is not possible to un-upload a file !
            # https://gitlab.com/gitlab-org/gitlab/issues/26075

            # jj['mardown'] has proper markdown code that can be used to
            # point to the file. We want to use that in an issue note that
            # points to the file.
            what = jj['markdown']
            text =  'Uploaded file {} by {} {}\n'.format(what, who, where)

            print("\tadding attachment info from %s to issue %d: %s" % (sender, server_id, summary))
        else:
            raise KeyError(m['type'])

        req=pr.post("/issues/{}/notes".format(server_id),
                expected_return=[201],
                body=text,
                created_at=m['date'],
                **kwargs
                )

    # modification date of the bug -- also need it to set the closing
    # date.
    api_keys=dict(updated_at=issue['last_modified_date'])

    if issue['status_name'] == 'Closed' or issue['status_name'] == 'RESOLVED':
        api_keys['state_event']='close'
    else:
        # It's a bit annoying but a raw "PUT" changing _only_ the
        # modification date won't work.
        api_keys['state_event']='reopen'

    print("\tupdating issue %d: %s" % (server_id, summary))
    pr.put("/issues/{}".format(server_id), [200], **api_keys)


def main_processing(pr, translator):
    trackers = get_list_of_trackers()
    print_list_of_trackers(trackers)
    for tracker in trackers:
        tracker_id,tracker_name=tracker
        print("Getting csv for %s" % tracker_name)
        issues = get_terse_descriptions_of_issues_for_tracker(tracker)
        if restrict_issues is not None:
            issues = [ i for i in issues if int(i['artifact_id']) in restrict_issues ]
        print_title_of_issues(issues, " in tracker \"%s\"" % tracker_name)
        for issue in issues:
            issue_id=int(issue['artifact_id'])
            tree = get_detail_page(issue_id)
            extract_more_fields_from_detail_page(issue, tree)
            issue['messages']=get_all_messages_of_issue(issue_id, tree=tree)
            issue['attachments']=get_all_attachments_of_issue(issue_id, tree=tree)
            transform_issue(tracker,issue,translator)
            if not dry_run:
                all_issue_iids.append(int(issue['artifact_id']))
                create_issue(pr, issue)


def print_help():
    help_text="""
Usage: CreateGitlabIssuesFromGforge.py [options]

Recognized options:
    -t, --accesstoken <gitlabPersonalAccessToken>
    -l, --labels <comma separated list of text label to add to gitlab issue> (may be given several times)
    -p, --gitlabprojectname <projectname (path) or numerical project id>
    -S, --gforgeserver <gforge server url, e.g. https://gforge.example.com/>
    -G, --gforgegroupid <numeric gforge project id>
    -g, --gitlabserver <gitlab api url, e.g. https://gitlab.example.com/api/v4>
    -i, --issues <comma separated list of gforge issue ids>
    -u, --userdatabase <path to json file with user matching table>
    --stealth temporarily remove project members while doing batch
    operation, so as to avoir mass e-mails
    -d, --debug   print calls to the gitlab api
    -w, --write   DO THE ACTIONS FOR REAL (otherwise, just test)
"""
    print(help_text)

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                "hwdt:l:p:G:i:g:u:S:",
                [
                    "write",
                    "stealth",
                    "debug",
                    "gforgeserver=",
                    "accesstoken=",
                    "label=",
                    "gitlabprojectname=",
                    "gforgegroupid=",
                    "issue=",
                    "issues=",
                    "gitlabserver=",
                    "userdatabase=",
                    ])
    except getopt.GetoptError as e:
        print(e)
        print_help()
        sys.exit(2)

    userdatabase_path = None
    debug = False

    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-t", "--accesstoken"):
            personalAccessToken = arg
        elif opt in ("-l", "--labels", "--label"):
            labels_always += arg.split(',')
        elif opt in ("-p", "--gitlabprojectname"):
            gitlabProjectName = arg
        elif opt in ("-S", "--gforgeserver"):
            gforge_toplevel = arg
        elif opt in ("-G", "--gforgegroupid"):
            gforge_group_id = int(arg)
        elif opt in ("-i", "--issue", "--issues"):
            restrict_issues = set([int(x) for x in arg.split(',')])
        elif opt in ("-w", "--write"):
            dry_run = False
        elif opt in ("--stealth"):
            stealth = True
        elif opt in ("-u", "--userdatabase"):
            userdatabase_path = arg
        elif opt in ("--gitlabserver"):
            gitlab_api_url = arg
        elif opt in ("-d", "--debug"):
            debug=True

    gl = gitlab_connection(gitlab_api_url, personalAccessToken, debug_api_calls=debug)

    pr = gl.project(gitlabProjectName, stealth=stealth, verbose=True)

    translator = user_translator(gl)

    if userdatabase_path is not None:
        translator.load_database(userdatabase_path)

    stopped=None
    try:
        if stealth and not dry_run:
            pr.temporarily_remove_members()
        main_processing(pr, translator)
    except Exception as e:
        print("\n*** Exception raised, entering rollback sequence ***\n")
        print(e)
        stopped=e
    finally:
        if stealth and not dry_run:
            pr.restore_members()

    if translator.translate_misses:
        print(report_error("The following issues were detected during processing. You might want to fix them before running with -w. For that, edit userdatabase.json and pass -u userdatabase.json"))
        print(",\n".join(translator.translate_misses))
        if not dry_run:
            print("Note that since you passed -w, issues were created even with these missed translations")
    else:
        if stopped is None:
            print(looks_good("Everything looks good"))
        if dry_run:
            print("This was just a dry run. Perhaps you want to run again with -w ?")
    if not dry_run and all_issue_iids and pr.has_owner_access():
        print(looks_good("If you're not satisfied with the results, you can delete the issues that have just been created with the following shell commands"))
        for i in all_issue_iids:
            print("curl  -X DELETE --header 'PRIVATE-TOKEN: {}' '{}'".format(
                personalAccessToken,
                "{}/projects/{}/issues/{}".format(
                    gitlab_api_url,
                    urllib.parse.quote(gitlabProjectName, safe=''),
                    i
                )))

    if stopped is not None:
        print("\n*** The following exception was raised during processing ***\n")
        raise stopped
