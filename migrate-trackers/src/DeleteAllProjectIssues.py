#!/usr/bin/env python3

import os
import sys
import getopt
import re
import requests
import urllib.parse
from lxml import etree
from io import StringIO,BytesIO
from datetime import datetime
import json
from gitlab_connection import gitlab_connection

# Everything here can be configured from the command line.

gitlabProjectName = 'some_group/some_project'
gitlab_api_url = 'https://gitlab.inria.fr/api/v4'
personalAccessToken = None

dry_run = True

def print_help():
    help_text="""
Usage: DeleteAllProjectIssues.py [options]

Recognized options:
    --accesstoken=<gitlabPersonalAccessToken>
    --gitlabprojectname=<projectname or project id>
    --gitlabserver=<gitlab api url, e.g. https://gitlab.example.com/api/v4>
    -d, --debug   print calls to the gitlab api
    --write   DO THE ACTIONS FOR REAL (otherwise, just test)

"""
    print(help_text)

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                "hdwt:p:g:",
                [
                    "accesstoken=",
                    "debug",
                    "write",
                    "gitlabprojectname=",
                    "gitlabserver=",
                    ])
    except getopt.GetoptError as e:
        print(e)
        print_help()
        sys.exit(2)
    
    debug = False

    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-t", "--accesstoken"):
            personalAccessToken = arg
        elif opt in ("-w", "--write"):
            dry_run = False
        elif opt in ("-p", "--gitlabprojectname"):
            gitlabProjectName = arg
        elif opt in ("--gitlabserver"):
            gitlab_api_url = arg
        elif opt in ("-d", "--debug"):
            debug=True

    gl = gitlab_connection(gitlab_api_url, personalAccessToken, debug_api_calls=debug)

    pr = gl.project(gitlabProjectName)

    if not pr.has_owner_access():
        if not dry_run:
            raise RuntimeError("You must have owner-level access to run this script")
        print("NOTE: your current access token will not allow you to do these operations, you must set $TOKEN to something with owner permission")
        personalAccessToken="$TOKEN"

    all_ids = [ int(x['iid']) for x in pr.get_paginated("/issues") ]

    for i in all_ids:
            if dry_run:
                print("curl  -X DELETE --header \"PRIVATE-TOKEN: {}\" '{}'".format(
                    personalAccessToken,
                    "{}/projects/{}/issues/{}".format(
                        gitlab_api_url,
                        urllib.parse.quote(gitlabProjectName, safe=''),
                        i
                    )))
            else:
                d_req = pr.delete("/issues/{}".format(i), [204])
