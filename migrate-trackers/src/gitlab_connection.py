import re
import requests
import json
from gitlab_project_connection import gitlab_project_connection

class gitlab_connection(object):
    def __init__(self, url, token, debug_api_calls=False):
        # simple provision if the "/api/v4" part is missing.
        url = re.sub("/$", "", url)
        if re.match("^https?://[^/]*$", url):
            url += "/api/v4"
        self.url = url
        self.token = token
        self.debug_api_calls = debug_api_calls
        self.check_gitlab_admin()

    def __pc(self, text):
        if not self.debug_api_calls:
            return
        print("\x1b[01;35m{}\x1b[00m".format(text))

    def nonapi_url(self):
        return re.sub("/api/v\d+$", "", self.url)

    def check_return(self, t, endpoint, req, expected_return):
        e = expected_return
        msg = ""
        has_error = e is not None and req.status_code not in e
        if has_error:
            msg = "gitlab {} {} returned unexpected code {}".format(
                    t, endpoint, req.status_code)
            msg += "; is your token correct ?"
        json_parsed = None
        try:
            # at least DELETE calls sometimes return an empty string
            if not re.match(b"^\s*$", req.content):
                json_parsed = json.loads(req.content)
        except json.decoder.JSONDecodeError as ex:
            if not has_error:
                msg += "gitlab {} {} returned malformed json:\n".format(
                        t, endpoint)
            else:
                msg += "\n" + "Malformed json returned:\n"
            msg += req.content.decode("UTF-8")
            has_error = True
        if has_error:
            if json_parsed is not None:
                msg += "\n" + json.dumps(json.loads(req.content), indent=2)
            raise RuntimeError(msg)
        return json_parsed

    def get(self, endpoint, expected_return=None, **kwargs):
        headers={}
        if self.token:
            headers["PRIVATE-TOKEN"]=self.token
        if 'sudo' in kwargs:
            headers['Sudo'] = kwargs['sudo']
            del kwargs['sudo']
        self.__pc(">>> GET {} [{}]".format(self.url + endpoint, headers))
        req = requests.get(self.url + endpoint, headers=headers)
        return self.check_return("GET", endpoint, req, expected_return)

    def delete(self, endpoint, expected_return=None, **kwargs):
        headers={}
        if self.token:
            headers["PRIVATE-TOKEN"]=self.token
        if 'sudo' in kwargs:
            headers['Sudo'] = kwargs['sudo']
            del kwargs['sudo']
        self.__pc(">>> DELETE {} [{}]".format(self.url + endpoint, headers))
        req = requests.delete(self.url + endpoint, headers=headers)
        return self.check_return("DELETE", endpoint, req, expected_return)

    def put(self, endpoint, expected_return=None, **kwargs):
        headers={}
        if self.token:
            headers["PRIVATE-TOKEN"]=self.token
        if 'sudo' in kwargs:
            headers['Sudo'] = kwargs['sudo']
            del kwargs['sudo']
        self.__pc(">>> PUT {} [{}] [{}]".format(self.url + endpoint, headers, kwargs))
        req = requests.put(self.url + endpoint,
                headers=headers,
                json=kwargs)
        return self.check_return("PUT", endpoint, req, expected_return)

    def post(self, endpoint, expected_return=None, files=None, **kwargs):
        headers={}
        if self.token:
            headers["PRIVATE-TOKEN"]=self.token
        if 'sudo' in kwargs:
            headers['Sudo'] = kwargs['sudo']
            del kwargs['sudo']
        self.__pc(">>> POST {} [{}] [{}]".format(self.url + endpoint, headers, kwargs))
        req = requests.post(self.url + endpoint,
                headers=headers,
                files=files,
                json=kwargs)
        return self.check_return("POST", endpoint, req, expected_return)

    def get_paginated(self, endpoint, **kwargs):
        headers={}
        if self.token:
            headers["PRIVATE-TOKEN"]=self.token
        if 'sudo' in kwargs:
            headers['Sudo'] = kwargs['sudo']
            del kwargs['sudo']
        headers["pagination"]='keyset'
        headers["per_page"]="50"
        url = self.url + endpoint
        while url is not None:
            req = requests.get(url, headers=headers)
            self.__pc(">>> GET {} [{}]".format(url, headers))
            j = self.check_return("GET", endpoint, req, [200])
            for u in j:
                yield u

            links = req.headers["Link"].split(", ")
            links = [x.split("; ") for x in links ]
            links = {x[1]:re.sub("^<(.*)>$", lambda x: x.group(1), x[0]) for x in links}
            url = links.get('rel="next"')

    def check_gitlab_admin(self):
        """
        This gets run when setting up the object
        """
        self.is_admin_user = False
        self.user = 'nobody'
        self.has_sudo = False

        if self.token:
            j = self.get("/user", [200])
            self.is_admin_user = str(j.get('is_admin','false')).lower() == 'true'
            self.user = j['username']
            if self.is_admin_user:
                j = self.get("/user?sudo={}".format(self.user), [200,403])
                self.has_sudo = 'error' not in j

        print("Current gitlab user: {}, is_admin: {}, has_sudo: {}".format(
            self.user, self.is_admin_user, self.has_sudo))

    def project(self, project, *args, **kwargs):
        return gitlab_project_connection(self, project, *args, **kwargs)
