import urllib.parse
import gitlab_connection
import re

def report_error(text):
    return "\x1b[01;31m{}\x1b[00m".format(text)

class gitlab_project_connection(object):
    def __init__(self, gl, project, stealth=False, verbose=False):
        self.gl = gl
        self.members={}
        self.group_members={}
        self.removed_members=[]
        self.enclosing_namespace=None
        self.name=project
        if self.gl.token:
            self.find_info()
            self.find_members(verbose=verbose)
            if stealth:
                self.find_group_members(verbose=verbose)

    def has_admin_access(self):
        return self.gl.is_admin_user

    def has_sudo_access(self):
        return self.gl.has_sudo

    def has_owner_access(self):
        """
        "Owner" access is defined only for the group, so it's rather a
        concept that is best understood by looking up at the group level
        """
        if self.has_admin_access():
            return True
        a = -10
        for scope in [ "project_access", "group_access" ]:
            pa = self.permissions[scope]
            if pa is not None:
                if pa["access_level"] >= a:
                    a = pa["access_level"]
        if a < 0:
            raise RuntimeError("This access token has no access to the gitlab project. We cannot proceed")
        
        return a >= 50

    def get(self, sub_endpoint, *args, **kwargs):
        """
        mangles the endpoint, and passes it to the gitlab object
        """
        p = urllib.parse.quote(self.name, safe='')
        e = "/projects/{}{}".format(p, sub_endpoint)
        return self.gl.get(e, *args, **kwargs)

    def get_paginated(self, sub_endpoint, *args, **kwargs):
        """
        mangles the endpoint, and passes it to the gitlab object
        """
        p = urllib.parse.quote(self.name, safe='')
        e = "/projects/{}{}".format(p, sub_endpoint)
        return self.gl.get_paginated(e, *args, **kwargs)

    def post(self, sub_endpoint, *args, **kwargs):
        """
        mangles the endpoint, and passes it to the gitlab object
        """
        p = urllib.parse.quote(self.name, safe='')
        e = "/projects/{}{}".format(p, sub_endpoint)
        return self.gl.post(e, *args, **kwargs)

    def put(self, sub_endpoint, *args, **kwargs):
        """
        mangles the endpoint, and passes it to the gitlab object
        """
        p = urllib.parse.quote(self.name, safe='')
        e = "/projects/{}{}".format(p, sub_endpoint)
        return self.gl.put(e, *args, **kwargs)

    def delete(self, sub_endpoint, *args, **kwargs):
        """
        mangles the endpoint, and passes it to the gitlab object
        """
        p = urllib.parse.quote(self.name, safe='')
        e = "/projects/{}{}".format(p, sub_endpoint)
        return self.gl.delete(e, *args, **kwargs)

    def find_members(self, verbose=False):
        """
        This gets run when setting up the object
        """
        if verbose:
            print("Now listing project members")
        for u in self.get_paginated("/members"):
            if verbose:
                print("\t{}; {}; access={}; {}{}".format(
                    u['id'],
                    u['username'],
                    u['access_level'],
                    u['name'],
                    " [CURRENT USER, admin:{}]".format(self.gl.is_admin_user) if u['username'] == self.gl.user else ""
                    ))
            self.members[u['id']]=u

    def find_info(self):
        """
        This gets run when setting up the object
        """
        j = self.get("")
        self.id = j["id"]
        self.visibility = j["visibility"]
        self.permissions = j["permissions"]
        self.namespace = j["namespace"]
        self.namespace_id = int(j["namespace"]["id"])

    def find_group_members(self, verbose=False):
        """
        This gets run when setting up the object, but only if stealth
        mode is active
        """
        if verbose:
            print("Now listing group members")
        e="/groups/{}/members".format(self.namespace_id)
        for u in self.gl.get_paginated(e):
            if verbose:
                print("\t{}; {}; access={}; {}{}".format(
                    u['id'],
                    u['username'],
                    u['access_level'],
                    u['name'],
                    " [CURRENT USER, admin:{}]".format(self.gl.is_admin_user) if u['username'] == self.gl.user else ""
                    ))
            self.group_members[u['id']] = u

    def temporarily_remove_members(self):
        for uid,u in self.members.items():
            if u['username'] == self.gl.user:
                continue

            # We have several problems with project group members.
            #
            # an API DELETE operation on a project bot does not work, it
            # returns a 403. EXCEPT if the bot tries to remove itself
            # from the group (204, then)

            # We can't tell apart normal users and bot accounts, as
            # project access tokens are not yet accessible via the API
            # https://gitlab.com/gitlab-org/gitlab/-/issues/238991

            # All we can resort to is regexp matching on the user name.
            # Sort of a poor man's solution.

            if re.match("^project_{}_bot".format(self.id), u['username']):
                print("Not removing user {}, who is apparently a bot user".format(u['username']))
                continue

            if uid in self.group_members:
                print("Not removing user {}, who is a member of the enclosing namespace {}".format(
                    u['username'], self.namespace["name"]))
                continue

            print("Temporarily removing user {}".format(u['username']))
            try:
                self.delete("/members/{}".format(u['id']), [204])
                self.removed_members.append(u)
            except Exception as e:
                print(report_error(e))
                raise

    def restore_members(self):
        for u in self.removed_members:
            print("Restoring user {} with access level {}".format(
                u['username'],
                u['access_level']))

            try:
                self.post("/members",
                        expected_return=[201],
                        user_id=u['id'],
                        access_level=u['access_level'])
            except RuntimeError as e:
                print(report_error(e))
                print("-- proceeding anyway, as this is a restore operation")
        self.removed_members=[]

