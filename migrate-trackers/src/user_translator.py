import re
from lxml import etree
import json
import requests
from io import StringIO,BytesIO

def report_error(text):
    return "\x1b[01;31m{}\x1b[00m".format(text)

class user_translator(object):
    def __init__(self, gl):
        self.known_users=[]
        self.translate_misses=[]
        self.gl = gl

    def load_database(self, path):
        """
        This reads the userdatabase.json (or any file, passed with -u, that
        follows the same format)
        """
        f=open(path)
        try:
            data=json.loads(f.read())
            if type(data) != list:
                raise RuntimeError("Your user database file {} should contain an array".format(path))
        except json.JSONDecodeError as e:
            print("Your user database file {} does not contain valid JSON".format(path))
            raise
        for u in data:
            if type(u) == str:
                continue
            if type(u) == list:
                v = dict(
                    gforge_login=u[0],
                    gforge_name=u[1],
                    gitlab_login=u[2])
                if len(u) >= 4:
                    v["gitlab_uid"]=u[3]
                u = v
            self.known_users.append(u)

    def translate(self, field=None,
            gforge_login=None, gforge_name=None, gitlab_login=None,
            default=None,
            format=None):
        """
        This returns a gforge-gitlab correspondence from the known_users list
        which is at the beginning of this file.  The matching is done on the
        first nonempty field among the ones passed, from the following list:
            - gforge_login, gforge_name, gitlab_login
        If 'field' is None, then a dictionary is returned with all the known
        fields in it.
        If 'field' is not None, then only the corresponding field is
        returned. The possible fields are the three fields above, plus:
            - gitlab_name or gitlab_uid as well (retrieved automatically from
              the gitlab api, and cached)
        If 'default' is not None, this object is returned in case of a
        non-successful match.
        If 'format' is not None, then this format string is applied to a
        successful string match. It is not used at all if field is None and a
        dictionary is returned.
        """
        passed_args = locals()
        d = None
        m = None
        for matcher in ['gforge_login', 'gforge_name', 'gitlab_login']:
            v = passed_args[matcher]
            if v:
                m = lambda u: u[matcher] == v
                # Use _only_ this matcher
                break
        if not m:
            raise RuntimeError("must pass one of gforge_login, gforge_name, gitlab_login")

        try:
            ass=[u for u in self.known_users if m(u)]
        except KeyError:
            print(self.known_users)
            raise

        if not ass:
            # if the matching query was passed with more than one bit of
            # information, return it with every bit that we already know.
            failed={}
            for matcher in ['gforge_login', 'gforge_name', 'gitlab_login']:
                v = passed_args[matcher]
                if v:
                    failed[matcher]=v
            failed_match=str(failed)
            if failed_match not in self.translate_misses:
                print(report_error("No match in known list for " + failed_match))
                # raise RuntimeError("")
                self.translate_misses.append(failed_match)
            if default is not None:
                return default

            return None

        d=ass[0]
                
        if not d:
            return None

        glogin = d['gitlab_login']

        if 'gitlab_uid' not in d or 'gitlab_name' not in d:
            # go to gitlab api, fetch the info, cache that right in the
            # known users list
            # note that we don't need the access token, apparently.

            j = self.gl.get("/users?username={}".format(glogin))

            if j:
                d['gitlab_uid']=int(j[0]['id'])
                d['gitlab_name']=j[0]['name']
            else:
                # This is a hack to work around
                # https://gitlab.com/gitlab-org/gitlab/-/issues/36077
                # We visit the user profile page to get his/her gitlab uid.

                url = "{}/users/{}".format(self.gl.nonapi_url(), glogin)

                print("Looking up {} to find gitlab_uid for {}".format(url, glogin))
                req = requests.get(url)
                if req.status_code == 200:
                    tree=etree.parse(BytesIO(req.content),etree.HTMLParser())
                    root=tree.getroot()
                    x=root.findall(".//meta[@property=\"og:image\"]")
                    if x:
                        x=x[0].attrib.get('content')
                    if x:
                        x=re.search("/system/user/avatar/(\d+)/avatar", x)
                    if x:
                        x=x.group(1)
                        print("Found correspondence {}->{}".format(glogin, x))
                        d['gitlab_uid']=int(x)
                        j = self.gl.get("/users/{}".format(x), [200])
                        if j:
                            d['gitlab_name']=j['name']

        if field is None:
            return d
        if field in d:
            if format is not None:
                return format.format(d[field])
            return d[field]
        if field in d:
            if format is not None:
                return format.format(d[field])
            return d[field]
        print(report_error("Field {} not found with gitlab_login={}".format(field, glogin)))
        if default is not None:
            return default

